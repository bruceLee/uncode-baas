/**
 * Copyright@xiaocong.tv 2012
 */
package cn.uncode.baas.server.utils;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author weijun.ye
 */
public class WebApplicationContextUtil {

    public static <T> T getBean(String name, Class<T> clz) {
        WebApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
        return (T) ctx.getBean(name);
    }

}
