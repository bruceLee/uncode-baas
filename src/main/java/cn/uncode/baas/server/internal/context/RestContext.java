package cn.uncode.baas.server.internal.context;

import cn.uncode.baas.server.acl.token.AccessToken;

import java.util.HashMap;

public class RestContext extends HashMap<String, Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6732312763880543546L;
	
	public static final String BUCKET = "bucket";
	
	public static final String TOKEN = "token";
	
	public void setBucket(String bucket){
		super.put(BUCKET, bucket);
	}
	
	public String getBucket(){
		return String.valueOf(super.get(BUCKET));
	}
	
	public boolean containsBucket(){
		return super.containsKey(BUCKET);
	}
	
	public void setToken(AccessToken accessToken){
		super.put(TOKEN, accessToken);
	}
	
	public AccessToken getToken(){
		return (AccessToken)super.get(TOKEN);
	}
	
	public boolean containsAccessToken(){
		return super.containsKey(TOKEN);
	}

}
