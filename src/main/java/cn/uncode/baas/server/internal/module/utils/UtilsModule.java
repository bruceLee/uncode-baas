package cn.uncode.baas.server.internal.module.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import cn.uncode.dal.internal.util.message.Messages;
import cn.uncode.baas.server.exception.ValidateException;
import cn.uncode.baas.server.internal.context.RestContextManager;
import cn.uncode.baas.server.internal.module.rest.IRestModule;
import cn.uncode.baas.server.utils.DataUtils;
import cn.uncode.baas.server.utils.HttpClientUtil;
import cn.uncode.baas.server.utils.MD5Utils;
import cn.uncode.baas.server.utils.MqttMessageUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UtilsModule implements IUtilsModule {
	
	@Autowired
	private IRestModule restModule;
	
	public IRestModule getRest(){
		return restModule;
	}
	
    public String md5(String value) throws Exception {
        return MD5Utils.encrypt(value);
    }

    public String currentTime() {
        // EEE MMM dd hh:mm:ss z yyyy
        // Wed Jul 16 15:01:15 CST 2014
        return new Date().toString();
    }
    
    public Object httpGet(String url){
    	HttpClientUtil h = new HttpClientUtil();
    	Object result = null;
		try {
			result = h.sendGetRequest(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
    }
    public Object httpPost(String url, Object param){
    	HttpClientUtil h = new HttpClientUtil();
    	Map<String, Object> map = DataUtils.convert2Map(param);
    	Object result = null;
		try {
			result = h.sendPostRequest(url, map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
    }
    
    public long serverTime() {
        return new Date().getTime();
    }
    
    public long activityLastTime(){
    	try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss").parse("2014-09-30 24:00:00.000").getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	Calendar calendar = Calendar.getInstance();
    	calendar.set(2014, 9, 30, 24, 0, 0);
    	return calendar.getTimeInMillis();
    }
    
    public void sendMqttMsg(Object param) throws ValidateException{
    	Map<String, Object> map = DataUtils.convert2Map(param);
    	String topic = null;
    	String content = null;
    	String client = null;
    	if(null != map && map.containsKey("topic")){
    		topic = String.valueOf(map.get("topic"));
    	}else{
    		topic = RestContextManager.getContext().getBucket();
    	}
    	if(null != map && map.containsKey("content")){
    		content = String.valueOf(map.get("content"));
    	}
    	if(null != map && map.containsKey("client")){
    		client = String.valueOf(map.get("client"));
    	}else{
    		client = RestContextManager.getContext().getBucket() + "_system";
    	}
    	if(StringUtils.isEmpty(topic) || StringUtils.isEmpty(content) || StringUtils.isEmpty(client)){
    		throw new ValidateException(Messages.getString("ValidateError.1", "Content topic and client"));
    	}
    	MqttMessageUtil.publish(topic, content, client);
    }
}
